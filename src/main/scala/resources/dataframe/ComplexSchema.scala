package resources.dataframe

import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

object ComplexSchema {
  def main(args: Array[String]): Unit = {
    val spark =
      SparkSession.builder()
        .appName("dataframe-ComplexSchema")
        .master("local[4]")
        .getOrCreate()

    import spark.implicits._

    //
    // Example 1: nested StructType for nested rows
    //

    val rows1 = Seq(
      Row(1, Row("a", "b"), 8.00, Row(1,2)),
      Row(2, Row("c", "d"), 9.00, Row(3,4))
    )

    val rows1Rdd = spark.sparkContext.parallelize(rows1, 4)

    val schema1 = StructType(
      Seq(
        StructField("id", IntegerType, true),
        StructField("s1", StructType(
          Seq(
            StructField("x", StringType, true),
            StructField("y", StringType, true)
          )
        ), true),
        StructField("d", DoubleType, true),
        StructField("s2", StructType(
          Seq(
            StructField("u", IntegerType, true),
            StructField("v", IntegerType, true)
          )
        ), true)
      )
    )
    println("Position of subfield 'd' is " + schema1.fieldIndex("d"))

    val df1 = spark.createDataFrame(rows1Rdd, schema1)

    ///print schema ///
    df1.printSchema()
    //dataframe with nested row
    df1.show()

    df1.select("s1.x").show()
    df1.select($"s1".getField("x")).show()




  }
}
