package resources.dataframe

import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

object DatasetConversion {
  case class Customer(id: Integer, name: String, state: String)

  def main(args: Array[String]): Unit = {
    val spark =
      SparkSession.builder()
        .appName("DataFrame-DatasetConversion")
        .master("local[4]")
        .getOrCreate()

    import spark.implicits._
    val customer = Seq(
      Customer(1, "Google", "US"),
      Customer(2, "facebook", "US")
    )

    // Create the DataFrame without passing through an RDD
    val customerDF : DataFrame = spark.createDataFrame(customer)

    customerDF.printSchema()


  }
}
