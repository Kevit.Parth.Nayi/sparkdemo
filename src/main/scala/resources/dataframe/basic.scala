package resources.dataframe

import org.apache.spark.sql.SparkSession

object basic {

  case class Customer(id: Integer, name: String, state: String)

  def main(args: Array[String]): Unit = {
    val spark =
      SparkSession.builder()
        .appName("DataFrame-Basic")
        .master("local[4]")
        .getOrCreate()

    import spark.implicits._

    val customer = Seq(
      Customer(1, "Google", "US"),
      Customer(2, "facebook", "US")
    )

    val customerDF = spark.sparkContext.parallelize(customer, 3).toDF()

    println(customerDF.toString())

    customerDF.printSchema()

    customerDF.show()

    customerDF.select("id").show()

    customerDF.filter($"state".equalTo("US")).show()
  }
}
