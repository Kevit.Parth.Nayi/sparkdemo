package resources.dataframe

import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

object ComplexSchemaMapType {
  def main(args: Array[String]): Unit = {
    val spark =
      SparkSession.builder()
        .appName("dataframe-ComplexSchemamap")
        .master("local[4]")
        .getOrCreate()

    import spark.implicits._

    val rows3 = Seq(
      Row(1, 8.00, Map("u" -> 1, "v" -> 2)),
      Row(2, 9.00, Map("x" -> 3, "y" -> 4, "z" -> 5))
    )
    val rows3Rdd = spark.sparkContext.parallelize(rows3, 4)

    val schema3 = StructType(
      Seq(
        StructField("id", IntegerType, true),
        StructField("d", DoubleType, true),
        StructField("m", MapType(StringType, IntegerType))
      )
    )

    val df3 = spark.createDataFrame(rows3Rdd, schema3)

    df3.printSchema()
    df3.show()
    df3.select($"id", size($"m").as("count")).show()

    df3.select($"id", explode($"m")).show()
    df3.select($"id", $"m.u").show()

  }
}

