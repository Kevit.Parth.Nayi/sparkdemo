package resources.dataframe
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

object ComeplexSchemaArrayType {
  def main(args: Array[String]): Unit = {
    val spark =
      SparkSession.builder()
        .appName("dataframe-ComplexSchema")
        .master("local[4]")
        .getOrCreate()

    import spark.implicits._
    val rows2 = Seq(
      Row(1, Row("a", "b"), 8.00, Array(1,2)),
      Row(2, Row("c", "d"), 9.00, Array(3,4,5))
    )

    val rows2Rdd = spark.sparkContext.parallelize(rows2, 4)

    val schema2 = StructType(
      Seq(
        StructField("id", IntegerType, true),
        StructField("s1", StructType(
          Seq(
            StructField("x", StringType, true),
            StructField("y", StringType, true)
          )
        ), true)
      )
    )
      .add(StructField("d", DoubleType, true))
      .add("a", ArrayType(IntegerType))

    val df2 = spark.createDataFrame(rows2Rdd, schema2)

    df2.printSchema()
    df2.show()

    println("Count elements of each array in the column")
    df2.select($"id", size($"a").as("count")).show()

    println("Explode the array elements out into additional rows")
    df2.select($"id", explode($"a").as("element")).show()

    println("Apply a membership test to each array in a column")
    df2.select($"id", array_contains($"a", 2).as("has2")).show()

    println("Use column function getItem() to index into array when selecting")
    df2.select($"id", $"a".getItem(2)).show()

  }

}
