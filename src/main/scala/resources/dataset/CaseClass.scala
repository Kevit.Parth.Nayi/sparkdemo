package resources.dataset
import org.apache.spark.sql.SparkSession

object CaseClass {
  case class Number(i: Int, english: String, french: String)

  def main(args: Array[String]): Unit = {
    val spark =
      SparkSession.builder()
        .appName("Dataset-CaseClass")
        .master("local[4]")
        .getOrCreate()

    import spark.implicits._

    val numbers = Seq(
      Number(1, "one", "un"),
      Number(2, "two", "deux"),
      Number(3, "three", "trois"))
    val numberDS = numbers.toDS()

    numberDS.where($"i" > 2).select($"english", $"french").show()

  }
}
