package resources.dataset
import org.apache.spark.sql.SparkSession


object basic {
  def main(args: Array[String]): Unit = {
    val spark =
      SparkSession.builder()
        .appName("Dataset-Basic")
        .master("local[4]")
        .getOrCreate()

    import spark.implicits._

    val s = Seq(10, 11, 12, 13, 14, 15)
    val ds = s.toDS()

    ds.columns.foreach(println(_))
    ds.dtypes.foreach(println(_))
    ds.printSchema()
    ds.where($"value" > 12).show()

  }
}
